<?php


use Firewox\PowerGIS\CoreEnum;
use Firewox\PowerGIS\Entities\MetadataEntity;
use Firewox\PowerGIS\Entities\UserEntity;
use Firewox\PowerGIS\PowerGISService;
use Firewox\PowerGIS\Response;
use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase
{

  const TOKEN = 'ACCESS_TOKEN';

  public function testGetCoreMetadata() {

    $service = new PowerGISService(
      'API_ENDPOINT',
      self::TOKEN
    );

    $response = $service->getCoreMetadata(CoreEnum::CORE_USERS) ?: new Response();
    /* @var MetadataEntity $data */
    $data = $response->getData(MetadataEntity::class) ?: new MetadataEntity();

    $this->assertSame('User', $data->getName());

  }

  public function testGetCoreData() {

    $service = new PowerGISService(
      'API_ENDPOINT',
      self::TOKEN
    );

    $response = $service->getCoreData(CoreEnum::CORE_USERS) ?: new Response();
    /* @var UserEntity $data */
    $data = @$response->getData(UserEntity::class)[0] ?: new UserEntity();

    $this->assertSame('3d101cd0-5f99-45f5-824f-6983e5f5d240', $data->getGuid());

  }

  public function testGetLayerMetadata() {

    $service = new PowerGISService(
      'API_ENDPOINT',
      self::TOKEN
    );

    $response = $service->getLayerMetadata('747244953273364') ?: new Response();
    /* @var MetadataEntity $data */
    $data = $response->getData(MetadataEntity::class) ?: new MetadataEntity();

    $this->assertSame('LOCATION', $data->getName());

  }

  public function testGetLayerData() {

    $service = new PowerGISService(
      'API_ENDPOINT',
      self::TOKEN
    );

    $response = $service->getLayerData('747244953273364', null, null, null, 'id asc', 0, 1) ?: new Response();
    /* @var UserEntity $data */
    $data = @$response->getData()[0] ?: [];

    $this->assertSame('e1f82b5f-979c-4b31-a555-a93655693f1e', @$data['guid']);

  }


}
