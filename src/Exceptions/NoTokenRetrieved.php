<?php

namespace Firewox\PowerGIS\Exceptions;

class NoTokenRetrieved extends \Exception
{

    public function __construct(){
        parent::__construct('No token retrieved.');
    }

}