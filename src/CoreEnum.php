<?php


namespace Firewox\PowerGIS;


class CoreEnum
{

  const CORE_USERS = 'users';
  const CORE_USER_LAYERS = 'userlayers';
  const CORE_LAYERS = 'layers';
  const CORE_CLASSIFIERS = 'classifiers';
  const CORE_THEMES = 'themes';

}