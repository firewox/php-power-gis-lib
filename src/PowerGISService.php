<?php

namespace Firewox\PowerGIS;

use Firewox\PowerGIS\Exceptions\NoResponse;
use Firewox\PowerGIS\Exceptions\NoTokenRetrieved;
use Firewox\PowerGIS\Exceptions\ServerErrorResponse;
use Firewox\PowerGIS\Exceptions\ServerRequestFailed;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Karriere\JsonDecoder\JsonDecoder;

class PowerGISService
{

  private $connect_endpoint;
  private $layer_endpoint;
  private $core_endpoint;
  private $config;
  private $accessToken;
  private $gisToken;

  public function __construct(string $apiEndpoint,
                              string $accessToken,
                              array $guzzleConfig = [])
  {

    $this->connect_endpoint = $apiEndpoint . '/v1/connect';
    $this->layer_endpoint = $apiEndpoint . '/v1/layer';
    $this->core_endpoint = $apiEndpoint . '/v1';

    // Set access token
    $this->accessToken = $accessToken;

    // Get config for guzzle
    $this->config = array_merge(
      [
        'verify' => false       // Ignore SSL errors by default
      ],
      $guzzleConfig
    );

  }


  /**
   * Get HTTP client for guzzle
   * @return Client
   */
  private function getHttpClient(): Client {
    return new Client($this->config);
  }


  private function connect(): string {

    if(!!$this->gisToken) return $this->gisToken;

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();

      $response = $client->get($this->connect_endpoint, [
        'headers' => [
          'Authorization' => 'Bearer ' . $this->accessToken
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        $decoded = $decoder->decode((string) $response->getBody(), Response::class);

        if(!$decoded || !$decoded->data) throw new NoTokenRetrieved();
        $this->gisToken = $decoded->data;
        return $this->gisToken;

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getCoreMetadata(string $code): ?Response {

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();

      $response = $client->get($this->core_endpoint . "/{$code}/metadata", [
        'headers' => [
          'X-Token' => $this->connect()
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getLayerMetadata(string $layerId): ?Response {

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();

      $response = $client->get($this->layer_endpoint . "/{$layerId}/metadata", [
        'headers' => [
          'X-Token' => $this->connect()
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getCoreData(string $code,
                              ?string $id = null,
                              ?array $expand = null,
                              ?string $query = null,
                              ?string $order = null,
                              ?int $offset = null,
                              ?int $limit = null,
                              ?bool $includeCount = null): ?Response {

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();
      $queryParams = [];

      if($expand != null) $queryParams['expand'] = join(',', $expand);
      if($query != null) $queryParams['query'] = $query;
      if ($order != null) $queryParams['order'] = $order;
      if($limit != null) $queryParams['limit'] = $limit;
      if($offset != null) $queryParams['offset'] = $limit;
      if($includeCount != null) $queryParams['count'] = $includeCount ? 'true' : 'false';

      $url = ($id != null ? "{$this->core_endpoint}/{$code}/{$id}?" : "{$this->core_endpoint}/{$code}?") . http_build_query($queryParams);

      $response = $client->get($url, [
        'headers' => [
          'X-Token' => $this->connect()
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getLayerData(string $layerId,
                              ?string $id = null,
                              ?array $expand = null,
                              ?string $query = null,
                              ?string $order = null,
                              ?int $offset = null,
                              ?int $limit = null,
                               ?bool $includeCount = null,
                               ?bool $includeGeometry = null): ?Response
  {

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();
      $queryParams = [];

      if ($expand != null) $queryParams['expand'] = join(',', $expand);
      if ($query != null) $queryParams['query'] = $query;
      if ($order != null) $queryParams['order'] = $order;
      if ($limit != null) $queryParams['limit'] = $limit;
      if ($offset != null) $queryParams['offset'] = $limit;
      if ($includeCount != null) $queryParams['count'] = $includeCount ? 'true' : 'false';
      if ($includeGeometry != null) $queryParams['geometry'] = $includeGeometry ? 'true' : 'false';

      $url = ($id != null ? "{$this->layer_endpoint}/{$layerId}/{$id}?" : "{$this->layer_endpoint}/{$layerId}?") . http_build_query($queryParams);

      $response = $client->get($url, [
        'headers' => [
          'X-Token' => $this->connect()
        ]
      ]);

      if ($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string)$response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }




}