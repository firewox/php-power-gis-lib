<?php


namespace Firewox\PowerGIS\Entities;

class TypeEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $description;

  /**
   * @var array|null
   */
  public $config;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string|null
   */
  public function getDescription(): ?string
  {
    return $this->description;
  }


  /**
   * @return array|null
   */
  public function getConfig(): ?array
  {
    return $this->config;
  }


}