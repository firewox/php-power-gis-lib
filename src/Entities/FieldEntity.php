<?php


namespace Firewox\PowerGIS\Entities;

use Karriere\JsonDecoder\JsonDecoder;

class FieldEntity
{

  /**
   * @var array|null
   */
  public $type;

  /**
   * @var string|null
   */
  public $description;


  /**
   * @return array|null
   */
  public function getType(): ?TypeEntity
  {
    if(!$this->type) return null;
    $decoder = new JsonDecoder();
    return $decoder->decodeArray($this->type, TypeEntity::class);
  }


  /**
   * @return string|null
   */
  public function getDescription(): ?string
  {
    return $this->description;
  }


}