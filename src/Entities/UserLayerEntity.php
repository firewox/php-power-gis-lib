<?php


namespace Firewox\PowerGIS\Entities;


use Karriere\JsonDecoder\JsonDecoder;

class UserLayerEntity extends CommonEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $layerid;

  /**
   * @var string|null
   */
  public $themeid;

  /**
   * @var array|null
   */
  public $layer;

  /**
   * @var array|null
   */
  public $theme;

  /**
   * @var array|null
   */
  public $classifiers;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }

  /**
   * @return string|null
   */
  public function getLayerId(): ?string
  {
    return $this->layerid;
  }

  /**
   * @return string|null
   */
  public function getThemeId(): ?string
  {
    return $this->themeid;
  }


  /**
   * @return array|null
   */
  public function getLayer(): ?LayerEntity
  {

    if(!$this->layer) return null;
    $decoder = new JsonDecoder();
    return $decoder->decodeArray($this->layer, LayerEntity::class);

  }


  /**
   * @return array|null
   */
  public function getTheme(): ?ThemeEntity
  {

    if(!$this->theme) return null;
    $decoder = new JsonDecoder();
    return $decoder->decodeArray($this->theme, ThemeEntity::class);

  }


  /**
   * @return array|null
   */
  public function getClassifiers(): ?array
  {

    if(!$this->classifiers) return null;

    return array_map(function(array $data) {
      $decoder = new JsonDecoder();
      return $decoder->decodeArray($data, ClassifierEntity::class);
    }, $this->classifiers);

  }


}