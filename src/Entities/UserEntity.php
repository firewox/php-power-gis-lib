<?php


namespace Firewox\PowerGIS\Entities;


class UserEntity extends CommonEntity
{

  /**
   * @var string|null
   */
  public $email;

  /**
   * @var string|null
   */
  public $fullname;

  /**
   * @var string|null
   */
  public $avatar;


  /**
   * @return string|null
   */
  public function getEmail(): ?string
  {
    return $this->email;
  }


  /**
   * @return string|null
   */
  public function getFullName(): ?string
  {
    return $this->fullname;
  }


  /**
   * @return string|null
   */
  public function getAvatar(): ?string
  {
    return $this->avatar;
  }

}