<?php


namespace Firewox\PowerGIS\Entities;


use Karriere\JsonDecoder\JsonDecoder;

class CommonEntity
{

  /**
   * @var int|null
   */
  public $id;

  /**
   * @var string|null
   */
  public $guid;

  /**
   * @var string|null
   */
  public $createdon;

  /**
   * @var string|null
   */
  public $lastmodifiedon;

  /**
   * @var string|null
   */
  public $lastmodifiedbyid;

  /**
   * @var string|null
   */
  public $createdbyid;

  /**
   * @var array|null
   */
  public $lastModifiedBy;

  /**
   * @var array|null
   */
  public $createdBy;


  /**
   * @return int|null
   */
  public function getId(): ?int
  {
    return $this->id;
  }


  /**
   * @return string|null
   */
  public function getGuid(): ?string
  {
    return $this->guid;
  }


  /**
   * @return string|null
   */
  public function getCreatedOn(): ?string
  {
    return $this->createdon;
  }


  /**
   * @return string|null
   */
  public function getLastModifiedOn(): ?string
  {
    return $this->lastmodifiedon;
  }

  /**
   * @return string|null
   */
  public function getLastModifiedById(): ?string
  {
    return $this->lastmodifiedbyid;
  }

  /**
   * @return string|null
   */
  public function getCreatedById(): ?string
  {
    return $this->createdbyid;
  }

  /**
   * @return UserEntity|null
   */
  public function getLastModifiedBy(): ?UserEntity
  {

    if(!$this->lastModifiedBy) return null;
    $decoder = new JsonDecoder();
    return $decoder->decodeArray($this->lastModifiedBy, UserEntity::class);

  }

  /**
   * @return UserEntity|null
   */
  public function getCreatedBy(): ?UserEntity
  {

    if(!$this->createdBy) return null;
    $decoder = new JsonDecoder();
    return $decoder->decodeArray($this->createdBy, UserEntity::class);

  }

}