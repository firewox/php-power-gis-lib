<?php


namespace Firewox\PowerGIS\Entities;


use Karriere\JsonDecoder\JsonDecoder;

class ClassifierEntity extends CommonEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $query;

  /**
   * @var string|null
   */
  public $style;

  /**
   * @var string|null
   */
  public $layerid;

  /**
   * @var array|null
   */
  public $layer;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string|null
   */
  public function getQuery(): ?string
  {
    return $this->query;
  }


  /**
   * @return string|null
   */
  public function getStyle(): ?string
  {
    return $this->style;
  }


  /**
   * @return string|null
   */
  public function getLayerId(): ?string
  {
    return $this->layerid;
  }


  /**
   * @return array|null
   */
  public function getLayer(): ?array
  {

    if(!$this->layer) return null;
    $decoder = new JsonDecoder();
    return $decoder->decodeArray($this->layer, LayerEntity::class);

  }




}