<?php


namespace Firewox\PowerGIS\Entities;


class ThemeEntity extends CommonEntity
{

  /**
   * @var string|null
   */
  public $name;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }

  /**
   * @return int|null
   */
  public function getId(): ?int
  {
    return $this->id;
  }


  /**
   * @return string|null
   */
  public function getGuid(): ?string
  {
    return $this->guid;
  }


  /**
   * @return string|null
   */
  public function getCreatedOn(): ?string
  {
    return $this->createdon;
  }


  /**
   * @return string|null
   */
  public function getLastModifiedOn(): ?string
  {
    return $this->lastmodifiedon;
  }



}