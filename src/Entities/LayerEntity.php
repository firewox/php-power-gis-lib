<?php


namespace Firewox\PowerGIS\Entities;


class LayerEntity extends CommonEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $type;

  /**
   * @var string|null
   */
  public $driver;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }

  /**
   * @return string|null
   */
  public function getType(): ?string
  {
    return $this->type;
  }

  /**
   * @return string|null
   */
  public function getDriver(): ?string
  {
    return $this->driver;
  }


}