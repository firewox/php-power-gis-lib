<?php


namespace Firewox\PowerGIS\Entities;

use Karriere\JsonDecoder\JsonDecoder;

class MetadataEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $description;

  /**
   * @var array|null
   */
  public $fields;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string|null
   */
  public function getDescription(): ?string
  {
    return $this->description;
  }


  /**
   * @return array|null
   */
  public function getFields(): ?array
  {

    if(!$this->fields) return null;

    $decoder = new JsonDecoder();
    $data = [];

    foreach($this->fields as $key => $value) {

      $data[$key] = $decoder->decodeArray($data, FieldEntity::class);

    }

    return $data;
  }


}